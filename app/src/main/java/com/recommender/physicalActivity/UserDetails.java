package com.recommender.physicalActivity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class UserDetails extends AppCompatActivity {

    AppCompatButton detailsSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        Intent intent = getIntent();

        String usermail = intent.getStringExtra("usermail");

        detailsSubmit = findViewById(R.id.userDetailsbtn);

        detailsSubmit.setOnClickListener(view -> {
            String height = ((EditText)findViewById(R.id.height)).getText().toString();
            String weight = ((EditText)findViewById(R.id.weight)).getText().toString();
            String gender = ((EditText)findViewById(R.id.gender)).getText().toString();
            String goal = ((EditText)findViewById(R.id.dailyGoal)).getText().toString();
            String age = ((EditText)findViewById(R.id.age)).getText().toString();

            FirebaseFirestore db = FirebaseFirestore.getInstance();

            Map<String, Object> user = new HashMap<>();
            user.put("height", height);
            user.put("weight", weight);
            user.put("gender", gender);
            user.put("goal", goal);
            user.put("age",age);

            // Add a new document with a generated ID
            db.collection("users").document(usermail)
                    .set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void unused) {
                    Intent intent = new Intent(UserDetails.this, RecommenderActivity.class);
                    intent.putExtra("height", height);
                    intent.putExtra("weight", weight);
                    intent.putExtra("gender", gender);
                    intent.putExtra("goal",goal);
                    intent.putExtra("age",age);
                    intent.putExtra("id", usermail);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                    startActivity(intent);
                    finish();
                }
            });
        });
    }
}