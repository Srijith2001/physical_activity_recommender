package com.recommender.physicalActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import lecho.lib.hellocharts.listener.ComboLineColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.ComboLineColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ComboLineColumnChartView;

public class RecommenderActivity extends AppCompatActivity implements SensorEventListener {

    Python py;
    PyObject module;
    PyObject pmodule;

    private SensorManager sensorManager;
    private float acceleration;

    private boolean running = false;
    private float totalSteps = 0f;
    private float previousTotalSteps = 0f;

    private TextView tv_stepTaken;
    private TextView hr_stepTaken;
    private CircularProgressBar hr_progress_circular;
    private CircularProgressBar tv_progress_circular;
    private Float[] act_plan = new Float[24];
    private int flag =1;
    private String currDay;
    private int currentDay;
    private int goal;
    private int total_step_difference;
    private int currTime;

    private ProgressDialog progressDialog;

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommender);

        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACTIVITY_RECOGNITION) == PackageManager.PERMISSION_DENIED){
            //ask for permission
            requestPermissions(new String[]{Manifest.permission.ACTIVITY_RECOGNITION}, 0);
        }

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        loadData();

        Intent intent = getIntent();

        // receive the value by getStringExtra() method
        // and key must be same which is send by first activity
        float h = Float.parseFloat(intent.getStringExtra("height"));
        int w = Integer.parseInt(intent.getStringExtra("weight"));
        String g = intent.getStringExtra("gender");
        goal = Integer.parseInt(intent.getStringExtra("goal"));
        int age = Integer.parseInt(intent.getStringExtra("age"));
        String id = intent.getStringExtra("id");
        int gender;
        if(g.equals("Male") || g.equals("M") || g.equals("m"))
              gender = 1;
        else
            gender=0;
        float bmi = w/(h*h);
        double[] features = {gender,age,h,w,bmi};
        int cluster = RandomForestClassifier.predict(features);

        System.out.println("Belongs to "+cluster);

        if (!Python.isStarted()) {
            Python.start(new AndroidPlatform(this));
        }

        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        py = Python.getInstance();
        module = py.getModule("get_activity_plan");
        pmodule = py.getModule("predictive_model");
        FirebaseFirestore db = FirebaseFirestore.getInstance();

        try {
            Calendar calendar = Calendar.getInstance();

            switch (calendar.get(Calendar.DAY_OF_WEEK)){
                case Calendar.SUNDAY:
                    // Current day is Sunday
                    currDay = "sunday";
                    currentDay = 3;
                    break;// Current day is Monday
                case Calendar.TUESDAY:
                    currDay = "tuesday";
                    currentDay=5;
                    break;
                case Calendar.WEDNESDAY:
                    currDay = "wednesday";
                    currentDay=6;
                    break;
                case Calendar.THURSDAY:
                    currDay = "thursday";
                    currentDay = 4;
                    break;
                case Calendar.FRIDAY:
                    currDay = "friday";
                    currentDay = 0;
                    break;
                case Calendar.SATURDAY:
                    currDay = "saturday";
                    currentDay = 2;
                    break;
                default: currDay = "monday"; currentDay=1; break;
            }

            module.callAttr("get_activity_plan", cluster, currDay, goal).toJava(byte[].class);

            act_plan  = module.callAttr("ret_act_plan").toJava(Float[].class);

            for(int i=0;i<24;i++){
                System.out.println(act_plan[i]);
            }
            currTime = calendar.get(Calendar.HOUR_OF_DAY);

            int stepDiff=0;
            for(int i=0;i<currTime;i++){
                stepDiff += act_plan[i].intValue();
            }
            lqr(act_plan,calendar.get(Calendar.HOUR_OF_DAY),stepDiff,goal);

            for(int i=0;i<24;i++){
                System.out.println(act_plan[i]);
            }

            total_step_difference = 0;

            Integer activityPatterGroup = module.callAttr("get_act_pattern",cluster,currDay).toJava(Integer.class);
            System.out.println("Python completed");
            updateUI(act_plan);

            Timer timer = new Timer ();
            TimerTask hourlyTask = new TimerTask () {
                @Override
                public void run () {
                    runOnUiThread(() -> {
                        if(calendar.get(Calendar.HOUR_OF_DAY)==0){
                            final boolean[] flag = {false};
                            final Float[] plan = new Float[24];
                            total_step_difference = 0;
                            // Get previous weeks activity pattern if available
                            db.collection("users").document(id).collection("Steps")
                                    .get()
                                    .addOnCompleteListener(task -> {
                                        if (task.isSuccessful()) {
                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                if(document.getId().equals(currDay)){
                                                    for(int i=0;i<24;i++) {
                                                        plan[i]=Float.parseFloat(Objects.requireNonNull(document.getString(((String.valueOf(i))))));
                                                    }
                                                    flag[0] =true;
                                                }
                                            }
                                        } else {
                                            flag[0] =false;
                                        }
                                    });
                            if(flag[0]){
                                act_plan  = plan;
                            }
                        }

                        //Check if opened for the first time
                        if(flag!=1) {
                            int curr_steps = resetSteps(currTime, currDay, id);
                            int step_diff = (int) (act_plan[currTime-1] - curr_steps);
                            total_step_difference += step_diff;
                            int hour_target_ach;
                            if(curr_steps>=act_plan[currTime - 1].intValue())
                                hour_target_ach = 1;
                            else hour_target_ach=0;
                            if (pmodule.callAttr("willTargetAchieve", currentDay, currTime, curr_steps, act_plan[currTime].intValue(), hour_target_ach, goal, cluster, activityPatterGroup).toJava(String.class).equals("0")){
                                lqr(act_plan, currTime, total_step_difference, goal);
                            }
                            updateUI(act_plan);
                        }
                        else{
                            hr_stepTaken = findViewById(R.id.hr_stepsTaken);
                            hr_stepTaken.setText("0");
                            hr_progress_circular = findViewById(R.id.hr_progress_circular);
                            hr_progress_circular.setProgressMax(act_plan[Calendar.getInstance().get(Calendar.HOUR_OF_DAY)].intValue());
                            hr_progress_circular.setProgressWithAnimation(0f);

                            tv_stepTaken = findViewById(R.id.tv_stepsTaken);
                            tv_stepTaken.setText("0");
                            tv_progress_circular = findViewById(R.id.tv_progress_circular);
                            tv_progress_circular.setProgressMax(goal);
                            tv_progress_circular.setProgressWithAnimation((float) 0f);

                            flag=0;
                        }
                        if(progressDialog.isShowing()) progressDialog.dismiss();
                    });
                }
            };

            // schedule the task to run starting now and then every hour...
            timer.schedule (hourlyTask, 0L, 1000*60*60);
        }
        catch(Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
            System.out.println(e.getMessage());
        }
    }

    private void updateUI(Float[] act_plan){

        Calendar calendar = Calendar.getInstance();
        currTime = calendar.get(Calendar.HOUR_OF_DAY);

        ComboLineColumnChartView chartView = findViewById(R.id.chart);
        chartView.setOnValueTouchListener(new ValueTouchListener());

        ComboLineColumnChartData data = getPlot(act_plan);

        chartView.setComboLineColumnChartData(data);

        TextView hr_target = findViewById(R.id.hr_totalMax);
        hr_target.setText(String.valueOf(act_plan[currTime].intValue()));

        TextView tv_target = findViewById(R.id.tv_totalMax);
        tv_target.setText(String.valueOf(goal));

    }
    private ComboLineColumnChartData getPlot(Float[] act_plan){
        List<Column> columns = new ArrayList<>();
        List<SubcolumnValue> values;
        for (int i=currTime;i<24;i++) {
            values = new ArrayList<SubcolumnValue>();
            values.add(new SubcolumnValue(act_plan[i].intValue(),Color.parseColor("#FF5733")));
            Column column = new Column(values);
            column.setHasLabels(true);
            column.setHasLabelsOnlyForSelected(true);
            columns.add(column);
        }
        ColumnChartData columnChartData = new ColumnChartData(columns);

        List<Line> lines = new ArrayList<>();
        List<PointValue> values_line = new ArrayList<>();
        int sum =0;
        for (int i = currTime; i <24; ++i) {
            sum+=act_plan[i].intValue()/8;
            values_line.add(new PointValue(i-currTime,sum));

            Line line = new Line(values_line);
            line.setColor(ChartUtils.COLOR_GREEN);
            line.setCubic(true);
            line.setHasPoints(false);
            lines.add(line);
        }

        LineChartData lineChartData = new LineChartData(lines);

        ComboLineColumnChartData data = new ComboLineColumnChartData(columnChartData,lineChartData);
        data.setAxisXBottom(null);
        data.setAxisYLeft(null);

        List<AxisValue> avalues = new ArrayList<AxisValue>();
        for (int value = 0; value < 24-currTime; value++) {
            AxisValue axisValue = new AxisValue(value);
            axisValue.setLabel(String.valueOf(value+currTime));
            avalues.add(axisValue);
        }

        Axis axisX = new Axis(avalues);

        Axis axisY = new Axis().setHasLines(true);
        axisY.setMaxLabelChars(5);
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisY);
        return data;
    }

    @Override
    protected void onStart() {
        super.onStart();
        running = true;
        Sensor stepSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (stepSensor==null){
            Toast.makeText(this,"No sensor detected on this device",Toast.LENGTH_SHORT).show();
        }
        else{
            sensorManager.registerListener(this, stepSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    @Override
    public void onAccuracyChanged(final Sensor sensor, int accuracy){
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        if(running){
            totalSteps = event.values[0];
            int currentSteps = ((int) totalSteps) - ((int) previousTotalSteps);
            hr_stepTaken = findViewById(R.id.hr_stepsTaken);
            hr_stepTaken.setText(""+ currentSteps);

            hr_progress_circular = findViewById(R.id.hr_progress_circular);
//            hr_progress_circular.setProgressMax(act_plan[Calendar.getInstance().get(Calendar.HOUR_OF_DAY)].intValue());
            hr_progress_circular.setProgressWithAnimation(((float) currentSteps));

            tv_stepTaken = findViewById(R.id.tv_stepsTaken);
            tv_stepTaken.setText(""+ ((int) totalSteps));
            tv_progress_circular = findViewById(R.id.tv_progress_circular);
//            tv_progress_circular.setProgressMax(goal);
            tv_progress_circular.setProgressWithAnimation((float) totalSteps);
            updateUI(act_plan);

        }
    }

    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER) );
    }

    public int resetSteps(int currHr, String currDay, String id) {
        previousTotalSteps = totalSteps;
        tv_stepTaken = findViewById(R.id.tv_stepsTaken);
        if(currHr==24)
            tv_stepTaken.setText("0");
        hr_stepTaken = findViewById(R.id.hr_stepsTaken);
        hr_stepTaken.setText("0");
        hr_progress_circular=findViewById(R.id.hr_progress_circular);
        hr_progress_circular.setProgressWithAnimation(0f);
        saveData(currHr,currDay,id);
        return (int) previousTotalSteps;
    }

    public void saveData(int currHr, String currDay, String id)
    {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> steps = new HashMap<>();
        steps.put(String.valueOf(currHr), (int)previousTotalSteps);
        db.collection("users").document(id).collection("Steps").document(currDay).set(steps);

        SharedPreferences sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat("key1",previousTotalSteps);
        editor.apply();
    }

    private void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
        float savedNumber = sharedPreferences.getFloat("key1",0f);
        Log.d("recommender", String.valueOf(savedNumber));
        previousTotalSteps = savedNumber;
    }

    private void lqr(Float[] activity_plan,int curr_hr,int step_diff,int target){
        for(int i=curr_hr;i<24;i++){
            activity_plan[i] = ((target+step_diff)*(activity_plan[i]))/target;
        }
    }
    private class ValueTouchListener implements ComboLineColumnChartOnValueSelectListener {

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onColumnValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
            Toast.makeText(RecommenderActivity.this, "Selected column: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPointValueSelected(int lineIndex, int pointIndex, PointValue value) {
            Toast.makeText(RecommenderActivity.this, "Selected line point: " + value, Toast.LENGTH_SHORT).show();
        }

    }
}