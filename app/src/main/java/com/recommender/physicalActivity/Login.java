package com.recommender.physicalActivity;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.shobhitpuri.custombuttons.GoogleSignInButton;

import java.util.Objects;

public class Login extends AppCompatActivity implements  View.OnClickListener {

    GoogleSignInButton signInButton;
    GoogleSignInClient mSignInClient;

    // Email Password Login
    AppCompatButton lgnButton;

//    ActivityBinding mBinding;
    private FirebaseAuth mAuth;
    private static final int SIGN_IN = 9001;

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        signInButton = findViewById(R.id.google_sign_in);
        signInButton.setOnClickListener(this);

        final TextView registerHereBtn = findViewById(R.id.registerHereBtn);

        registerHereBtn.setOnClickListener(v -> {
            startActivity(new Intent(Login.this, Register.class));
            finish();
        });

        //Email Password sign-in
        String email = findViewById(R.id.email).toString();
        String password = findViewById(R.id.password).toString();
        lgnButton = findViewById(R.id.loginBtn);
        lgnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        startActivity(new Intent(Login.this,UserDetails.class));
                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Incorrect Email/Password", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        // if user logged in, go to sign-in screen
        if (mAuth.getCurrentUser() != null) {
            DocumentReference docRef = db.collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser().getEmail()));
            docRef.get().addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        Intent intent= new Intent(Login.this, RecommenderActivity.class);
                        String height = document.getData().get("height").toString();
                        String weight = document.getData().get("weight").toString();
                        String gender = document.getData().get("gender").toString();
                        String goal = document.getData().get("goal").toString();
                        String age = document.getData().get("age").toString();
                        intent.putExtra("height", height);
                        intent.putExtra("weight", weight);
                        intent.putExtra("gender", gender);
                        intent.putExtra("goal",goal);
                        intent.putExtra("age",age);
                        intent.putExtra("id", mAuth.getCurrentUser().getEmail());
                        startActivity(intent);
                        finish();
                    } else {
                        Intent intent = new Intent(Login.this,UserDetails.class);
                        intent.putExtra("usermail",mAuth.getCurrentUser().getEmail());
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            });
        }
        if(progressDialog.isShowing()) progressDialog.dismiss();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.google_sign_in) {
            signIn();
        }
    }

    private void signIn() {
        Intent signInIntent = mSignInClient.getSignInIntent();
        startActivityForResult(signInIntent,SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SIGN_IN){
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            firebaseAuthWithGoogle(account.getIdToken());
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount signedIn) {
        if (signedIn != null) {
            // sigin is successful
            signInButton.setVisibility(View.GONE);
            Intent intent = new Intent(Login.this, UserDetails.class);
            intent.putExtra("usermail",signedIn.getEmail());
            startActivity(intent);
        } else {
            // signin is cancelled
            signInButton.setVisibility(View.VISIBLE);
        }
    }

    private void updateUIF(FirebaseUser signedIn) {
        if (signedIn.getUid() != null) {
            // signin is successfull
            signInButton.setVisibility(View.GONE);
        } else {
            // signin is cancelled
            signInButton.setVisibility(View.VISIBLE);
        }
    }

    private void firebaseAuthWithGoogle(String idToken) {
        AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUIF(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            Snackbar.make(mBinding.mainLayout, "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }
}