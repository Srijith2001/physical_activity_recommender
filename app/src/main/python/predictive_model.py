import pickle
from os.path import dirname, join

def willTargetAchieve(day,h_num,cSteps,h_tar,h_t_ac,daily_tar,cgroup,ap):
    # performing predictions on the test dataset
    filename = join(dirname(__file__), "libs/predictive_model.sav")
    loaded_model = pickle.load(open(filename, 'rb'))
    test = [[day,h_num,cSteps,h_tar,h_t_ac,daily_tar,cgroup,ap]]
    y_pred = loaded_model.predict(test)
    if y_pred[0]==0:
        return "0"
    return "1"